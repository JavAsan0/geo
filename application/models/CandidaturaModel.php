<?php
  class CandidaturaModel extends CI_Model //CI_Model ya viene con el framework
  {
    function __construct()
    {
      // Reconocer a las clases
      parent::__construct();
    }
    //Funcion para insertar un instructor de MYSQL
    function insertar($datos){
      //Active Record en CodeIgniter
      return $this->db->insert("candidato",$datos);

    }
    //Funcion para consultar Candidatos
    function obtenerTodos(){
      $listadoCandidatos=$this->db->get("candidato"); //Devuelve un array   SIEMPRE VALIDAR CON UN IF
      if($listadoCandidatos->num_rows()>0){ //SI HAY DATOS     num_rows nos deuelve el numero de filas que haya
        return $listadoCandidatos->result();
      }else{ //NO HAY DATOS
        return false;
      }
    }
    function borrar($id_can){
      $this->db->where("id_can",$id_can);
      if ($this->db->delete("candidato")) {
        return true;
      } else {
        return false;
      }
    }
  }// Cierre de la clase
 ?>
