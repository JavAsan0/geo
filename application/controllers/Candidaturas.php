<?php
  class Candidaturas extends CI_Controller
  {
    function __construct()
    {
      parent:: __construct();
      //error_reporting(0);

      //Cargar aqui todos los modelos
      $this->load-> model('CandidaturaModel');
    }
    public function lista(){
      $data['candidaturas']=$this->CandidaturaModel->obtenerTodos();
      $this->load->view('header');
      $this->load->view('candidaturas/lista',$data);
      $this->load->view('footer');
    }
    public function nuevo(){
      $this->load->view('header');
      $this->load->view('candidaturas/nuevo');
      $this->load->view('footer');
    }
    public function guardar(){
      $datosNuevoCandidato=array(
        "cedula_can"=>$this->input->post('cedula_can'),
        "dignidad_can"=>$this->input->post('dignidad_can'),
        "apellidos_can"=>$this->input->post('apellidos_can'),
        "nombres_can"=>$this->input->post('nombres_can'),
        "edad_can"=>$this->input->post('edad_can'),
        "movimiento_can"=>$this->input->post('movimiento_can'),
        "ideologia_can"=>$this->input->post('ideologia_can'),
        "latitud_can"=>$this->input->post('latitud_can'),
        "longitud_can"=>$this->input->post('longitud_can')
      );
      if($this->CandidaturaModel->insertar($datosNuevoCandidato)){
        redirect('candidaturas/lista');
      }else{
        echo "<h1>ERROR AL INSERTAR</H1>"; //EMBEBER CODIGO
      }
    }

    //Funcion para eliminar Candidato
    public function eliminar($id_can){
      if ($this->CandidaturaModel->borrar($id_can)) {
        redirect('candidaturas/lista');
      } else {
        echo "ERROR AL BORRAR :(";
      }
    }


      //
      //print_r($datosNuevoInstructor);
      // echo $this->input->post('cedula_ins'); //Post es seguro y get es inseguro, se recomienda usar POST, AQUI SE CAPTURA
      // echo "<br>"; //embeber codigo, poner codigo de un tipo en otro... poner html en php
      // $this->input->post('primer_apellido_ins');
      // echo "<br>";
      // echo $this->input->post('segundo_apellido_ins');
      // echo "<br>";
      // echo $this->input->post('nombres_ins');
      // echo "<br>";
      // echo $this->input->post('titulo_ins');
      // echo "<br>";
      // echo $this->input->post('telefono_ins');
      // echo "<br>";
      // echo $this->input->post('direccion_ins');

  }//Cierre de la clase
 ?>
